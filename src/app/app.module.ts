import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './componentos/list/list.component';
import { AddComponent } from './componentos/add/add.component';
import { FormsModule } from '@angular/forms';
import { ValidarYearDirective } from './directives/validar-year.directive';
import { NewPipePipe } from './new-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    AddComponent,
    ValidarYearDirective,
    NewPipePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
