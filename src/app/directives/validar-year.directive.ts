import { Directive } from '@angular/core';
import { AbstractControl, FormControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[appValidarYear]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: ValidarYearDirective, multi: true }
  ]
})
export class ValidarYearDirective {
  private _control!: AbstractControl;

  constructor() { }

  
  validate(control: FormControl): ValidationErrors | null {
    console.log(this._control);

    if (control.value < 2024 && control.value > 1900){
      return null;
    }else {
      return { 'notValid': true };
    }
  }

}
