import { Component, SimpleChanges } from '@angular/core';
import { ManagebooksService } from './services/managebooks.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'M06examenUF3';
  numberOfBooks:number;

  constructor(private manageBooks:ManagebooksService) {
    this.numberOfBooks=0;

  }

ngOnInit() {

      this.manageBooks.registerMessage.subscribe(message => {
        console.log("message "+ message);
        this.numberOfBooks=message;

        
      })


}

/* 
ngOnChanges(changes: SimpleChanges): void {
 

} */

} 




