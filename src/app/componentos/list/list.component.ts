import { Component } from '@angular/core';
import { Llibre } from 'src/app/models/llibre.model';

import { ManagebooksService } from 'src/app/services/managebooks.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  libros: Llibre[] = [];
  filter1:string;
  selectFilter:string;
  rangeFilter:number;

  filteredLibros: Llibre[] = [];
  maxYear:number;
  minYear:number;

  constructor(private manageBooks:ManagebooksService){
    this.libros=[];
    this.filter1="";
    this.selectFilter="";
    this.rangeFilter=0;
    this.filteredLibros = this.libros;
    this.maxYear=1900;
    this.minYear=2024;

  }

  ngOnInit(): void {
    this.libros=this.manageBooks.getBookList();
    this.filteredLibros = this.libros;

    for (let index = 0; index < this.libros.length; index++) {
      let libro= this.libros[index];
      if (libro.any > this.maxYear){
        this.maxYear = libro.any;
      };
      
    }
    for (let index = 0; index < this.libros.length; index++) {
      let libro = this.libros[index];
      if (libro.any < this.minYear){
        this.minYear = libro.any;
      };
  }
}

filter() {
  console.log(this.filter1);
  console.log(this.selectFilter);
  console.log(this.rangeFilter);
  this.filteredLibros = this.libros.filter((llibre: { nom: string, categoria: string, any: number }) => {
    return llibre.nom.indexOf(this.filter1) != -1 &&
           llibre.categoria.indexOf(this.selectFilter) != -1 &&
           llibre.any <= this.rangeFilter;
  });
}

filterInput() {
  this.filteredLibros = this.libros.filter((llibre: { nom: string }) => {
      if (llibre.nom.indexOf(this.filter1) != -1) {
          return true;
      }
      return false;
  }); 

}

filterSelect() {
  console.log(this.selectFilter);
  this.filteredLibros = this.libros.filter((llibre: { categoria: string }) => {
      if (llibre.categoria.indexOf(this.selectFilter) != -1) {
          return true;
      }
      return false;
  });
}
delete(libro:Llibre){
  this.manageBooks.delete(libro);
  this.libros=this.manageBooks.getBookList();
  this.filteredLibros = this.libros;

}

}
