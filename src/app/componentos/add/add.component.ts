import { Component } from '@angular/core';
import { Llibre } from 'src/app/models/llibre.model';
import { ManagebooksService } from 'src/app/services/managebooks.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {

  libro!:Llibre;
 
 categories:string[]=["ficcion","documental","poesia"];
 languages:string[]=["castellano","ingles","chino"];

  constructor(private managebook:ManagebooksService){
    this.libro = new Llibre();
    


  }


  addBook(){
    console.log(this.libro);
    this.managebook.addBookToArray(this.libro)


  }
}
