import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './componentos/add/add.component';
import { ListComponent } from './componentos/list/list.component';

const routes: Routes = [
  {path:"",
  component:AddComponent},
  
  {path:"list",
  component:ListComponent},
  
  {path:"add",
  component:AddComponent},];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

