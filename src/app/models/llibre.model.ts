export class Llibre {
    nom: string;
    descripcio: string;
    categoria: string;
    any: number;
    idioma: string;
    prestec: boolean;
    preu: number; 
    
    
  
    constructor() {
      this.nom = "";
      this.descripcio = "";
      this.categoria = "";
      this.any = 1900;
      this.idioma = "";
      this.prestec = false;
      this.preu = 0; 
      
    }
}
