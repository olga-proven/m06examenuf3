import { Injectable } from '@angular/core';
import { Llibre } from '../models/llibre.model';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ManagebooksService {
  libros: Llibre[] = [];


  private messageBooksNumber = new BehaviorSubject(5);
  registerMessage = this.messageBooksNumber.asObservable();


  constructor() {

    this.libros = [
      {
        nom: "book1",
        descripcio: "novela",
        categoria: "documental",
        any: 1976,
        idioma: "ingles",
        prestec: false,
        preu: 76
      },
      {
        nom: "book2",
        descripcio: "novela",
        categoria: "ficcion",
        any: 1934,
        idioma: "chino",
        prestec: false,
        preu: 16
      },

      {
        nom: "book3",
        descripcio: "novela",
        categoria: "ficcion",
        any: 1934,
        idioma: "castellano",
        prestec: false,
        preu: 55
      },

      {
        nom: "book4",
        descripcio: "nuevo autor",
        categoria: "poesia",
        any: 2021,
        idioma: "catalan",
        prestec: true,
        preu: 23
      },

      {
        nom: "book5",
        descripcio: "ganador Nobel",
        categoria: "poesia",
        any: 2001,
        idioma: "ingles",
        prestec: false,
        preu: 43
      },
    ];

  }

  ngOnInit() {
    this.sendNumber(this.libros.length);
    console.log(this.libros.length);

  }


  sendNumber(num: number) {
    this.messageBooksNumber.next(num);
  }

  getBookList() {
    return this.libros



  }

  delete(libro: Llibre) {

    console.log(libro);
    for (let index = 0; index < this.libros.length; index++) {
      let libroInArray = this.libros[index];
      if (libroInArray.nom == libro.nom) {
        this.libros.splice(index, 1)

      };
    }
    this.sendNumber(this.libros.length);


  }

  addBookToArray(libro: Llibre) {
    this.libros.push(libro);
    this.sendNumber(this.libros.length);


  }




}
